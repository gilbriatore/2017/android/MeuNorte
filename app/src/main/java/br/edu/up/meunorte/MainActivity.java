package br.edu.up.meunorte;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity
    implements SensorEventListener {

  private float anguloAtual = 0f;
  private float[] mBussola = new float[3];
  private float[] mRotacao = new float[9];
  private float[] mGravidade = new float[3];
  private float[] mOrientacao = new float[3];
  private ImageView imageView;
  private SensorManager sm;
  private Sensor acelerometro;
  private Sensor bussola;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    imageView = (ImageView) findViewById(R.id.imageView);
    sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    acelerometro = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    bussola = sm.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
  }

  @Override
  protected void onPause() {
    super.onPause();
    sm.unregisterListener(this, acelerometro);
    sm.unregisterListener(this, bussola);
  }

  @Override
  protected void onResume() {
    super.onResume();
    sm.registerListener(this, acelerometro, SensorManager.SENSOR_DELAY_NORMAL);
    sm.registerListener(this, bussola, SensorManager.SENSOR_DELAY_NORMAL);
  }

  @Override
  public void onSensorChanged(SensorEvent event) {

    if (event.sensor == acelerometro) {
      mGravidade = event.values.clone();
    } else if (event.sensor == bussola) {
      mBussola = event.values.clone();
    }

    SensorManager.getRotationMatrix(mRotacao, null, mGravidade, mBussola);
    SensorManager.getOrientation(mRotacao, mOrientacao);

    float radianos = mOrientacao[0];
    float graus = (float) (Math.toDegrees(radianos) + 360) % 360;

    RotateAnimation animation = new RotateAnimation(
        anguloAtual, -graus,
        Animation.RELATIVE_TO_SELF, 0.5f,
        Animation.RELATIVE_TO_SELF, 0.5f);
    animation.setDuration(250);
    animation.setFillAfter(true);
    imageView.startAnimation(animation);
    anguloAtual = -graus;
  }

  @Override
  public void onAccuracyChanged(Sensor sensor, int accuracy) {
    //Não implementado.
  }
}
